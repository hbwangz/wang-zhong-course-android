package cn.td0f7.android.wangzhongcourse.entity;

public class People {

    private int head;
    private String name;

    public People() {
    }

    public People(int head, String name) {
        this.head = head;
        this.name = name;
    }

    public int getHead() {
        return head;
    }

    public String getName() {
        return name;
    }

    public void setHead(int head) {
        this.head = head;
    }

    public void setName(String name) {
        this.name = name;
    }
}