package cn.td0f7.android.wangzhongcourse.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyIntentService extends IntentService {
    private final String TAG = getClass().getName();

    public MyIntentService() {//必须实现
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {//必须重写
        //Intent是从Activity发过来的
        String action = intent.getExtras().getString("param");//获取参数等信息
        if (action.equals("a")) Log.i(TAG, "启动serviceA");
        else if (action.equals("b")) Log.i(TAG, "启动serviceB");
        else if (action.equals("c")) Log.i(TAG, "启动serviceC");

        //让服务休眠1秒
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind");
        return super.onBind(intent);
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void setIntentRedelivery(boolean enabled) {
        super.setIntentRedelivery(enabled);
        Log.i(TAG, "setIntentRedelivery");
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

}