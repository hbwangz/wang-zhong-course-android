package cn.td0f7.android.wangzhongcourse;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import cn.td0f7.android.wangzhongcourse.activity.DemoActivity;
import cn.td0f7.android.wangzhongcourse.base.BaseActivity;
import cn.td0f7.android.wangzhongcourse.broadcastreceiver.BroadcastReceiverActivity;
import cn.td0f7.android.wangzhongcourse.contentprovider.ContentProviderActivity;
import cn.td0f7.android.wangzhongcourse.demo.Demo01Activity;
import cn.td0f7.android.wangzhongcourse.netword.NetworkRequestActivity;
import cn.td0f7.android.wangzhongcourse.service.ServiceActivity;

public class MainActivity extends BaseActivity {
    private Context context = MainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("intent的值", (getIntent().getExtras() == null) + "");
        TextView tv = findViewById(R.id.tv1);
        tv.requestFocus();
        Button btn = findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "按钮点击了", Toast.LENGTH_SHORT).show();
            }
        });

        CheckBox cb = findViewById(R.id.cb);
//        cb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(MainActivity.this, "1", Toast.LENGTH_SHORT).show();
//            }
//        });
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(MainActivity.this, "" + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        RadioButton rb = findViewById(R.id.rb);
        rb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(MainActivity.this, "" + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        ImageView iv = findViewById(R.id.iv);

        Glide.with(context)
                .load("https://img2.baidu.com/it/u=86150580,3675278099&fm=26&fmt=auto&gp=0.jpg")
                .placeholder(getResources().getDrawable(R.drawable.loading))
                .error(getResources().getDrawable(R.drawable.error))
                .into(iv);

        findViewById(R.id.btProgress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ProgressBarActivity.class));
            }
        });

        findViewById(R.id.btSeekBar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, SeekBarActivity.class));
            }
        });

        findViewById(R.id.btRatingBar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, RatingBarActivity.class));
            }
        });

        findViewById(R.id.btAlertDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, AlertDialogActivity.class));
            }
        });

        findViewById(R.id.btListView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ListViewActivity.class));
            }
        });

        findViewById(R.id.btRecycler).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, RecyclerActivity.class));
            }
        });

        findViewById(R.id.btThread).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ThreadActivity.class));
            }
        });

        findViewById(R.id.btPopupWindow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, PopupWindowActivity.class));
            }
        });

        findViewById(R.id.btSpinner).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, SpinnerActivity.class));
            }
        });

        findViewById(R.id.btIntent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("cn.td0f7.action.intent.demo");
                intent.putExtra("content", "传参测试 推动学堂");
//                context.startActivity(intent);
                startActivityForResult(intent, 1);
            }
        });

        findViewById(R.id.btIconFont).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, IconFontActivity.class));
            }
        });

        findViewById(R.id.btDemo1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, Demo01Activity.class));
            }
        });

        findViewById(R.id.btActivity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DemoActivity.class));
            }
        });

        findViewById(R.id.btContentProvider).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ContentProviderActivity.class));
            }
        });

        findViewById(R.id.btService).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ServiceActivity.class));
            }
        });

        findViewById(R.id.btBroadcastReceiver).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, BroadcastReceiverActivity.class));
            }
        });


        findViewById(R.id.btNet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, NetworkRequestActivity.class));
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1://新增返回值
                if (resultCode == 0) {
                    Toast.makeText(context, "新增失败!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, data.getExtras().getString("result"), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}