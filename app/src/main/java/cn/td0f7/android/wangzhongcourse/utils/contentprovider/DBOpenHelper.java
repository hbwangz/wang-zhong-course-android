package cn.td0f7.android.wangzhongcourse.utils.contentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper {

    final String CREATE_SQL = "CREATE TABLE user(_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT)";
    final String CREATE_SQL2 = "CREATE TABLE user2(_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT)";
    final String CREATE_SQL3 = "CREATE TABLE user3(_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT)";

    public DBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        if (oldVersion == 1 && newVersion == 2)
            db.execSQL(CREATE_SQL2);
    }

}