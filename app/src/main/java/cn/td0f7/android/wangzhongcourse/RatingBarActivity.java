package cn.td0f7.android.wangzhongcourse;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Rating;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Toast;

public class RatingBarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_bar);

        RatingBar ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setNumStars(8);
        ratingBar.setRating(0f);
        ratingBar.setStepSize(1f);
        ScrollView scrollView = findViewById(R.id.scrollView);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Toast.makeText(RatingBarActivity.this, "1", Toast.LENGTH_SHORT).show();
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
        });
    }
}