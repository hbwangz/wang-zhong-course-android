package cn.td0f7.android.wangzhongcourse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

public class PopupWindowActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnShow;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_window);
        btnShow = findViewById(R.id.btnShow);
        tv = findViewById(R.id.tv);

        btnShow.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnShow:
                View view = getLayoutInflater().inflate(R.layout.popup_window_item, null, false);
                Button btnCancel = view.findViewById(R.id.btnCancel);
                PopupWindow popupWindow = new PopupWindow(this);

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (popupWindow != null && popupWindow.isShowing())
                            popupWindow.dismiss();
                    }
                });

                popupWindow.setContentView(view);
                popupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                popupWindow.setFocusable(true);

                popupWindow.showAsDropDown(tv, 10, 10);
//                popupWindow.showAtLocation(tv, Gravity.CENTER, 0, 0);
                break;
        }
    }

}