package cn.td0f7.android.wangzhongcourse;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AlertDialogActivity extends AppCompatActivity {
    private Context context = AlertDialogActivity.this;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_dialog);
        findViewById(R.id.btAlert1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("提示！");
                builder.setMessage("是否要删除？");

                builder.create().show();
            }
        });

        findViewById(R.id.btAlert2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("提示！");
                builder.setMessage("是否要删除？");
                builder.setNegativeButton("取消", null);
                builder.setPositiveButton("确认", null);
                builder.setCancelable(false);//点击空白处或返回键不能关闭此弹框

                AlertDialog alertDialog = builder.create();

                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Button negative = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        negative.setTextColor(Color.GREEN);
                        negative.setBackgroundColor(Color.GRAY);
                    }
                });

                alertDialog.show();
            }
        });

        findViewById(R.id.btAlertList).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence items[] = new CharSequence[3];
                items[0] = "1";
                items[1] = "2";
                items[2] = "3";

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("提示！");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context, items[which], Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setIcon(getResources().getDrawable(R.drawable.loading));
                builder.setNegativeButton("取消", null);
                builder.setPositiveButton("确认", null);
                builder.setCancelable(false);//点击空白处或返回键不能关闭此弹框

                builder.create().show();
            }
        });

        findViewById(R.id.btCus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View view = getLayoutInflater().inflate(R.layout.cus, null, false);
                Button button = view.findViewById(R.id.bt);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(AlertDialogActivity.this, "1", Toast.LENGTH_SHORT).show();
                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                    }
                });
                builder.setTitle("自定义布局");
                builder.setView(view);
                builder.setCancelable(false);

                alertDialog = builder.create();

                alertDialog.show();
            }
        });
    }
}