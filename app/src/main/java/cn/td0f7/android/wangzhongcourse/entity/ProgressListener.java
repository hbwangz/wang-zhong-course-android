package cn.td0f7.android.wangzhongcourse.entity;

public interface ProgressListener {
    void onProgress(ProgressModel model);
}
