package cn.td0f7.android.wangzhongcourse.entity;

public class ProgressModel {
    private long current;//当前下载进度
    private long lenght;//总进度
    private boolean done;//是否完成

    public ProgressModel(long current, long lenght, boolean done) {
        this.current = current;
        this.lenght = lenght;
        this.done = done;
    }

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public long getLenght() {
        return lenght;
    }

    public void setLenght(long lenght) {
        this.lenght = lenght;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
