package cn.td0f7.android.wangzhongcourse.base;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

public class MyIconFontView extends TextView {
    public MyIconFontView(Context context) {
        super(context);
        setFont(context);
    }

    public MyIconFontView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public MyIconFontView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MyIconFontView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setFont(context);
    }

    private void setFont(Context context) {
        Typeface type = Typeface.createFromAsset(context.getAssets(), "iconfont.ttf");
        setTypeface(type);
    }
}
