package cn.td0f7.android.wangzhongcourse.utils;

import androidx.annotation.NonNull;

import java.io.IOException;

import cn.td0f7.android.wangzhongcourse.base.MyResponseBody;
import cn.td0f7.android.wangzhongcourse.entity.ProgressListener;
import okhttp3.Callback;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OkhttpUtil {
    private static OkHttpClient.Builder client = new OkHttpClient.Builder();

    public static void download(ProgressListener listener, Callback callback) {
        client.addNetworkInterceptor(new Interceptor() {
            @NonNull
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                return response.newBuilder().body(new MyResponseBody(response.body(), listener)).build();
            }
        });

        String url = "https://tdui.td0f7.cn/ImagePreview.png";
        Request request = new Request.Builder().url(url).build();
        client.build().newCall(request).enqueue(callback);
    }
}
