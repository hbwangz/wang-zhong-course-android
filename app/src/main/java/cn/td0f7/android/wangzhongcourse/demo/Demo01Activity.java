package cn.td0f7.android.wangzhongcourse.demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cn.td0f7.android.wangzhongcourse.R;
import cn.td0f7.android.wangzhongcourse.base.BaseActivity;

public class Demo01Activity extends BaseActivity {
    private Context context = this;
    private Button btnLogin;
    private EditText etUser, etPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo01);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        } else {
            getActionBar().hide();
        }
        etUser = findViewById(R.id.etUser);
        etPass = findViewById(R.id.etPass);
        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUser.getText().toString().isEmpty()) {
                    Toast.makeText(context, "账号必须输入！", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etPass.getText().toString().isEmpty()) {
                    Toast.makeText(context, "密码必须输入！", Toast.LENGTH_SHORT).show();
                    return;
                }
                startActivity(new Intent(context, DemoList01Activity.class));
            }
        });
    }
}