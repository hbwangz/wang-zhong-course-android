package cn.td0f7.android.wangzhongcourse

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ProgressBar

class ProgressBarActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_progress_bar)
        val view = findViewById<ProgressBar>(R.id.progressBar)
        view.max = 100
        view.progress = 50
        view.secondaryProgress = 70
    }
}