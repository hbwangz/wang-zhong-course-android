package cn.td0f7.android.wangzhongcourse.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import cn.td0f7.android.wangzhongcourse.MainActivity;
import cn.td0f7.android.wangzhongcourse.R;
import cn.td0f7.android.wangzhongcourse.base.BaseActivity;
import cn.td0f7.android.wangzhongcourse.utils.AppManager;

public class Demo2Activity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo2);
    }
}