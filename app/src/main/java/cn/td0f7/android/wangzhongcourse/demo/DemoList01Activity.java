package cn.td0f7.android.wangzhongcourse.demo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import cn.td0f7.android.wangzhongcourse.R;
import cn.td0f7.android.wangzhongcourse.entity.Dog;

public class DemoList01Activity extends AppCompatActivity {
    private Context context = this;
    private List<Dog> dogList;//保存原始数据
    private List<Dog> dogs;//要显示的数据
    private List<Dog> popups;//弹框要显示的数据
    private MyAdapter myAdapter;
    private PopupAdapter popupAdapter;
    private RecyclerView lv;
    private EditText etName;
    private Button btShow;
    private PopupWindow popupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_list01);
        lv = findViewById(R.id.lv);
        etName = findViewById(R.id.etName);
        dogs = new ArrayList<>();
        dogList = new ArrayList<>();
        btShow = findViewById(R.id.btShow);
        popups = new ArrayList<>();
        myAdapter = new MyAdapter();
        popupAdapter = new PopupAdapter();
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        lv.setAdapter(myAdapter);
        lv.setLayoutManager(manager);
        lv.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));

        Dog dog = new Dog("雪橇犬", "阿拉斯加");
        dogList.add(dog);
        dog = new Dog("雪橇犬", "哈士奇");
        dogList.add(dog);
        dog = new Dog("雪橇犬", "萨摩耶");
        dogList.add(dog);

        dog = new Dog("咱也不知道咋说", "狼狗");
        dogList.add(dog);
        dog = new Dog("咱也不知道咋说", "柴犬");
        dogList.add(dog);

        dog = new Dog("测试", "1");
        dogList.add(dog);
        dog = new Dog("测试", "12");
        dogList.add(dog);
        dog = new Dog("测试", "123");
        dogList.add(dog);
        dogs.addAll(dogList);
        popups.addAll(dogList);

        myAdapter.notifyDataSetChanged();

        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //改变前
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //改变中
            }

            @Override
            public void afterTextChanged(Editable s) {
                //改变后
                String val = etName.getText().toString();
                dogs.clear();//清空集合所有数据
                if (val.isEmpty()) {
                    dogs.addAll(dogList);
                } else {
                    for (Dog item : dogList) {
                        if (item.getName().contains(val)) {
                            dogs.add(item);
                        }
                    }
                }
                myAdapter.notifyDataSetChanged();
            }
        });

        btShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show();
            }
        });
    }

    private void show() {
        popups.clear();
        popups.addAll(dogList);
        View view = getLayoutInflater().inflate(R.layout.activity_demo_list01, null, false);
        popupWindow = new PopupWindow(context);
        popupWindow.setContentView(view);
        RecyclerView lv = view.findViewById(R.id.lv);
        view.findViewById(R.id.btShow).setVisibility(View.GONE);
        EditText etName = view.findViewById(R.id.etName);

        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        lv.setLayoutManager(manager);
        lv.setAdapter(popupAdapter);
        lv.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));

        popupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);

        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //改变前
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //改变中
            }

            @Override
            public void afterTextChanged(Editable s) {
                //改变后
                String val = etName.getText().toString();
                popups.clear();//清空集合所有数据
                if (val.isEmpty()) {
                    popups.addAll(dogList);
                } else {
                    for (Dog item : dogList) {
                        if (item.getName().contains(val)) {
                            popups.add(item);
                        }
                    }
                }
                popupAdapter.notifyDataSetChanged();
            }
        });

        popupWindow.showAtLocation(this.etName, Gravity.CENTER, 0, 0);
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.VH> {
        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.dog_item, parent, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull VH holder, int position) {
            holder.tvType.setText("品种：" + dogs.get(position).getType());
            holder.tvName.setText("名称：" + dogs.get(position).getName());
        }

        @Override
        public int getItemCount() {
            return dogs == null ? 0 : dogs.size();
        }

        class VH extends RecyclerView.ViewHolder {
            private TextView tvType, tvName;

            public VH(@NonNull View itemView) {
                super(itemView);
                tvType = itemView.findViewById(R.id.tvType);
                tvName = itemView.findViewById(R.id.tvName);
            }
        }
    }

    class PopupAdapter extends RecyclerView.Adapter<PopupAdapter.VH> {
        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.dog_item, parent, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull VH holder, @SuppressLint("RecyclerView") int position) {
            holder.tvType.setText("品种：" + popups.get(position).getType());
            holder.tvName.setText("名称：" + popups.get(position).getName());

            holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (popupWindow != null && popupWindow.isShowing()) {
                        popupWindow.dismiss();
                    }
                    Toast.makeText(context, popups.get(position).getName(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return popups == null ? 0 : popups.size();
        }

        class VH extends RecyclerView.ViewHolder {
            private TextView tvType, tvName;
            private LinearLayout ll;

            public VH(@NonNull View itemView) {
                super(itemView);
                ll = itemView.findViewById(R.id.ll);
                tvType = itemView.findViewById(R.id.tvType);
                tvName = itemView.findViewById(R.id.tvName);
            }
        }
    }
}