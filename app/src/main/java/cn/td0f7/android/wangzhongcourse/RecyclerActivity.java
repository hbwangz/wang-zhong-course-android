package cn.td0f7.android.wangzhongcourse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecyclerActivity extends AppCompatActivity {
    private Context context = RecyclerActivity.this;
    private RecyclerView lvDemo;
    private List<Map<String, Object>> listitem;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        myAdapter = new MyAdapter();
        listitem = new ArrayList<>();
        for (int i = 0; i < 150; i++) {
            Map<String, Object> showitem = new HashMap<String, Object>();
            showitem.put("name", "name" + i);
            if (i % 2 == 0) {
                showitem.put("content", "content" + i);
            } else {
                showitem.put("content", "contentcontentcontent" + i);
            }
            showitem.put("check", false);
            listitem.add(showitem);
        }
        lvDemo = findViewById(R.id.lvDemo);
        lvDemo.setAdapter(myAdapter);
//        LinearLayoutManager manager = new LinearLayoutManager(context);
        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
//        manager.setOrientation(LinearLayoutManager.VERTICAL);
        lvDemo.setLayoutManager(manager);
//        lvDemo.addItemDecoration(new DividerItemDecoration(context, GridLayoutManager.VERTICAL));
    }

    class MyAdapter extends RecyclerView.Adapter<VH> {
        @NonNull
        @NotNull
        @Override
        public VH onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.recycler_item, parent, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull VH holder, int position) {
            holder.cb.setOnCheckedChangeListener(null);
            holder.cb.setChecked((Boolean) listitem.get(position).get("check"));
            holder.tvName.setText(listitem.get(position).get("name") + "");
            holder.tvContent.setText(listitem.get(position).get("content") + "");

            holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
                }
            });

            holder.cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    listitem.get(position).put("check", isChecked);
                }
            });
        }

        @Override
        public int getItemCount() {
            return listitem == null ? 0 : listitem.size();
        }
    }

    class VH extends RecyclerView.ViewHolder {
        private TextView tvName, tvContent;
        private CheckBox cb;
        private LinearLayout ll;

        public VH(@NonNull @NotNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvContent = itemView.findViewById(R.id.tvContent);
            cb = itemView.findViewById(R.id.cb);
            ll = itemView.findViewById(R.id.ll);
        }
    }
}