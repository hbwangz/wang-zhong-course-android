package cn.td0f7.android.wangzhongcourse.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String data = intent.getExtras().getString("data");
        Toast.makeText(context, data, Toast.LENGTH_SHORT).show();
    }
}
