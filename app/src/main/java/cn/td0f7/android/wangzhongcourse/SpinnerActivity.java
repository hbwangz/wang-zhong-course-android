package cn.td0f7.android.wangzhongcourse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.td0f7.android.wangzhongcourse.entity.People;

public class SpinnerActivity extends AppCompatActivity {
    private Spinner spinner_people;
    private List<People> data;
    private MyAdapter myAdadpter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        spinner_people = (Spinner) findViewById(R.id.spinner_people);
        data = new ArrayList<>();
        data.add(new People(R.mipmap.ic_launcher, "张三"));
        data.add(new People(R.mipmap.ic_launcher, "李四"));
        data.add(new People(R.mipmap.ic_launcher, "王五"));
        myAdadpter = new MyAdapter();
        spinner_people.setAdapter(myAdadpter);
        //代码省略看视频
    }

    class MyAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return data == null ? 0 : data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.spinner_item, parent, false);
            TextView tv_name = view.findViewById(R.id.tv_name);
            tv_name.setText(data.get(position).getName());
            return view;
        }
    }
}