package cn.td0f7.android.wangzhongcourse.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import cn.td0f7.android.wangzhongcourse.MainActivity;
import cn.td0f7.android.wangzhongcourse.R;
import cn.td0f7.android.wangzhongcourse.base.BaseActivity;
import cn.td0f7.android.wangzhongcourse.utils.AppManager;

public class DemoActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        startActivity(new Intent(this, Demo2Activity.class));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        AppManager.getAppManager().finishAllActivity();
        startActivity(new Intent(this, MainActivity.class));
    }
}