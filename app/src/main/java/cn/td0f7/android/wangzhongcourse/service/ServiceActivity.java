package cn.td0f7.android.wangzhongcourse.service;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import cn.td0f7.android.wangzhongcourse.R;

public class ServiceActivity extends AppCompatActivity {
    private Context context = this;
    private Button btnGet, btnSet;
    private Intent intent;
    private Intent bindIntent;
    private BindService.MyBinder binder;
    private final String TAG = getClass().getName();
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            binder = (BindService.MyBinder) service;
            Log.d(TAG, "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        btnGet = findViewById(R.id.btnGet);
        btnSet = findViewById(R.id.btnSet);

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binder != null) {
                    binder.setCount(0);
                    Toast.makeText(context, binder.getCount() + "", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binder != null)
                    Toast.makeText(context, binder.getCount() + "", Toast.LENGTH_SHORT).show();
            }
        });

        intent = new Intent();
        intent.setAction("cn.td0f7.android.service.start.SERVICE1");

        bindIntent = new Intent();
        bindIntent.setAction("cn.td0f7.android.service.bind.SERVICE2");

        bindService(bindIntent, connection, Service.BIND_AUTO_CREATE);
        startService(intent);

        Intent intent1 = new Intent("cn.td0f7.android.service.intent.SERVICE3");
        intent1.putExtra("param", "a");

        Intent intent2 = new Intent("cn.td0f7.android.service.intent.SERVICE3");
        intent2.putExtra("param", "b");

        Intent intent3 = new Intent("cn.td0f7.android.service.intent.SERVICE3");
        intent3.putExtra("param", "c");

//接着启动多次IntentService,每次启动,都会新建一个工作线程,但始终只有一个IntentService实例，也就是说在IntentService做了三个事情，但是没有重复创建IntentService
        startService(intent1);
        startService(intent2);
        startService(intent3);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(intent);
        unbindService(connection);
    }
}