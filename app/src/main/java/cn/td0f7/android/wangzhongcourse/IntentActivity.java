package cn.td0f7.android.wangzhongcourse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class IntentActivity extends AppCompatActivity {
    private TextView tv;
    private Button btnReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
        btnReturn = findViewById(R.id.btnReturn);
        setResult(0);
        tv = findViewById(R.id.tv);
        Log.d("intent的值", (getIntent().getExtras() == null) + "");
        tv.setText(getIntent().getExtras().getString("content"));

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                intent.putExtra("result", "我是返回值");
                setResult(1, intent);
                finish();
            }
        });
    }
}