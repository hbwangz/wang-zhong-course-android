package cn.td0f7.android.wangzhongcourse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListViewActivity extends AppCompatActivity {
    private Context context = this;
    private ListView lvDemo;
    private MyAdapter myAdapter = new MyAdapter();
    private List<Map<String, Object>> listitem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        lvDemo = findViewById(R.id.lvDemo);
//        //要显示的数据
//        String[] strs = {"基神", "B神", "翔神", "曹神", "J神"};
//        //创建ArrayAdapter
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_expandable_list_item_1, strs);
//        lvDemo.setAdapter(adapter);

        listitem = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < 50; i++) {
            Map<String, Object> showitem = new HashMap<String, Object>();
            showitem.put("name", "name" + i);
            showitem.put("content", "context" + i);
            showitem.put("check", false);
            listitem.add(showitem);
        }
//
//        SimpleAdapter adapter = new SimpleAdapter
//                (context, listitem, R.layout.list_item, new String[]{"name", "content"}, new int[]{R.id.name, R.id.content});
//        lvDemo.setAdapter(adapter);

        lvDemo.setAdapter(myAdapter);
        TextView header = new TextView(context);
        header.setText("Header");
        TextView footer = new TextView(context);
        footer.setText("Footer");
        lvDemo.addHeaderView(header);
        lvDemo.addFooterView(footer);

        footer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lvDemo.setSelection(0);
            }
        });

        lvDemo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listitem == null ? 0 : listitem.size();
        }

        @Override
        public Object getItem(int position) {
            return listitem.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            ViewHolder viewHolder;
            if (view == null) {
                viewHolder = new ViewHolder();
                if (position % 2 == 0) {
                    view = getLayoutInflater().inflate(R.layout.base_list_item1, parent, false);
                } else {
                    view = getLayoutInflater().inflate(R.layout.base_list_item2, parent, false);
                }
                viewHolder.name = view.findViewById(R.id.name);
                viewHolder.content = view.findViewById(R.id.content);
                viewHolder.checkBox = view.findViewById(R.id.checkBox);

                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.checkBox.setOnCheckedChangeListener(null);
            Log.d("当前" + position, "渲染");
            viewHolder.checkBox.setChecked((Boolean) listitem.get(position).get("check"));

            viewHolder.name.setText(listitem.get(position).get("name") + "");
            viewHolder.content.setText(listitem.get(position).get("content") + "");

            viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d("当前" + position, isChecked + "");
                    listitem.get(position).put("check", isChecked);
                }
            });
            return view;
        }
    }

    class ViewHolder {
        TextView name, content;
        CheckBox checkBox;
    }
}