# WangZhongCourse-Android

#### 介绍
Android原生开发教程代码演练源码
本教程哔哩哔哩免费更新中，赞助教程提前一周更新
赞助地址：https://www.td0f7.cn/course/3/Android-yuan-sheng-kai-fa-jiao-cheng-2021
哔哩哔哩：https://www.bilibili.com/video/BV1kq4y1Q7DN/

#### 知识点
本套课程设计到大部分知识点，包含但不限于以下
Android概念了解
Android五大布局
基础组件
动画
自定义View
活动 Activitie
服务 Service
广播 Broadcast Receiver
数据存储 Content Provider
进阶 Jetpack
声明式渲染 Compose
屏幕适配
Rxjava
Okhttp
Retrofit
项目实战


#### 特技

预计今年更新绝大部分教程，每周更新2-5课，谢谢大家支持。
